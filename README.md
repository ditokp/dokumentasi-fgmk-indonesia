<h1>Tentang</h1>
<p>Repositori ini merupakan repositori resmi dari situs <a href="https://fgmktutorindo.blogspot.com/">FGMK Tutor Indo</a> yang berisi file tutorial-tutorial yang berada di situs FGMK Tutor Indo dan dokumentasi-dokumentasi dari aplikasi <a href="https://github.com/ericoporto/fgmk">FGMK</a> dalam bahasa Indonesia.</p><br>
<h1>Lisensi</h1>
<p>Semua file tutorial dan dokumentasi disini baik gambar ataupun tulisan berlisensi <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.id">CC By-SA 4.0</a>.</p><br>
<h1>Kontribusi</h1>
<p>Jika Anda ingin berkontribusi, Anda bisa meng-fork repositori ini dan menambahkan dokumentasi atau tutorial-tutorial dan melakukan pull request. Atau Anda juga bisa mengirimkan file dokumentasi atau tutorial ke email saya : ditokurniap@merahputih.id.</p>
<p>Tutorial-tutorial atau dokumentasi yang dikirim ke sini akan di lisensi kan ke dalam CC By-SA 4.0 dengan nama pembuat tutorial atau dokumentasi Anda sendiri, bukan saya namanya karena dikirim oleh Anda.</p><br>
<h1>Kontak</h1>
<p>Email : ditokurniap@merahputih.id<br>
Mastodon : @DitoKurniaPratama@idevs.id</p>
